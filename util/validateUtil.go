package util

import (
	"errors"
	"fmt"
	"time"
)

// IsNotEmpty devuelve error si un string es vacio
func IsNotEmpty(data string, field string) error {
	if len(data) == 0 {
		resp := fmt.Sprintf("%s%s", field, " is required")
		return errors.New(resp)
	}
	return nil
}

// IsGreaterThanCeroInt devuelve error si un numero es menor que cero
func IsGreaterThanCeroInt(number int, field string) error {
	if number <= 0 {
		resp := fmt.Sprintf("%s%s", field, " is required")
		return errors.New(resp)
	}
	return nil
}

// IsGreaterThanCeroFloat64 devuelve error si un numero es menor que cero
func IsGreaterThanCeroFloat64(number float64, field string) error {
	if number <= 0 {
		resp := fmt.Sprintf("%s%s", field, " is required")
		return errors.New(resp)
	}
	return nil
}

// IsGreaterOrEqualToCeroInt devuelve error si un numero es menor o igual a cero
func IsGreaterOrEqualToCeroInt(number int, field string) error {
	if number < 0 {
		resp := fmt.Sprintf("%s%s", field, " is required")
		return errors.New(resp)
	}
	return nil
}

// IsGreaterOrEqualToCeroFloat64 devuelve error si un numero es menor o igual a cero
func IsGreaterOrEqualToCeroFloat64(number float64, field string) error {
	if number < 0 {
		resp := fmt.Sprintf("%s%s", field, " is required")
		return errors.New(resp)
	}
	return nil
}

// IsGreaterThanInt devuelve error si numeroA es menor que numeroB
func IsGreaterThanInt(numberA int, numberB int, field string) error {
	if numberA <= numberB {
		resp := fmt.Sprintf("%s%s", field, " is required")
		return errors.New(resp)
	}
	return nil
}

// IsGreaterThanFloat64 devuelve error si numeroA es menor que numeroB
func IsGreaterThanFloat64(numberA float64, numberB float64, field string) error {
	if numberA <= numberB {
		resp := fmt.Sprintf("%s%s", field, " is required")
		return errors.New(resp)
	}
	return nil
}

// IsGreaterOrEqualToInt devuelve error si numeroA es menor o igual que numeroB
func IsGreaterOrEqualToInt(numberA int, numberB int, field string) error {
	if numberA < numberB {
		resp := fmt.Sprintf("%s%s", field, " is required")
		return errors.New(resp)
	}
	return nil
}

// IsGreaterOrEqualToFloat64 devuelve error si numeroA es menor o igual que numeroB
func IsGreaterOrEqualToFloat64(numberA float64, numberB float64, field string) error {
	if numberA < numberB {
		resp := fmt.Sprintf("%s%s", field, " is required")
		return errors.New(resp)
	}
	return nil
}

// IsDateEmpty devuelve error si una fecha es vacia
func IsDateEmpty(data time.Time, field string) error {
	// if data == time.Time{} {
	// 	resp := fmt.Sprintf("%s%s", field, " is required")
	// 	return errors.New(resp)
	// }
	return nil
}
